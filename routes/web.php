<?php

Route::group(['middleware' => 'auth'], function() {
	Route::resource('/quotes', 'QuoteController', ['except' => ['index', 'show'] ]);
	Route::post('/quotes-comment/{id}', 'QuoteCommentController@store');
	Route::put('/quotes-comment/{id}', 'QuoteCommentController@update');
	Route::get('/quotes-comment/{id}/edit', 'QuoteCommentController@edit');
	Route::delete('/quotes-comment/{id}', 'QuoteCommentController@destroy');
	Route::get('/like/{type}/{model}', 'LikeController@like');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/quotes/filter/{tag}', 'QuoteController@filter');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile/{id?}', 'HomeController@profile');
Route::get('/quotes/random', 'QuoteController@random');
Route::get('/verify/{token}/{id}', 'Auth\RegisterController@verify_register');
Route::resource('/quotes', 'QuoteController', ['only' => ['index', 'show'] ]);
