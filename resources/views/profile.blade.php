@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row padding-profile">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h2 class="text-center" style="margin-bottom: 15px;">{{ $user->name }}</h2>
				<ul class="list-group">
					@foreach($user->quotes as $quote)
						<li class="list-group-item"><a href="/quotes/{{ $quote->slug }}">{{ $quote->title }}</a></li>
					@endforeach
				</ul>
			</div>
			<di class="col-md-3"></di>
		</div>
	</div>
@endsection