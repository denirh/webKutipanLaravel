@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="bg-thumb-city">
				@if(session('msg'))
					<div class="alert alert-success">
						<p>{{ session('msg') }}</p>
					</div>
				@endif
					<div class="title-city">
						<h3>Daftar Quotes</h3>
					</div>
					<div class="random text-center">
						<a href="/quotes" class="button-random">All</a>
						<a href="/quotes/random" class="button-random">Random</a> 
						<a href="/quotes/create" class="button-random">Create</a>
					</div>
				
				  <div class="row">
				  		<div class="col-12 col-md-9">
				  			<div class="row">
						  @foreach($quotes as $quote)
							  	<div class="col-sm-4">
							    	<div class="card">
							      		<div class="card-body">
							        		<h5 class="card-title" style="margin-bottom: 10px;">{{ $quote->title }}</h5>
							        		<p style="font-size: 12px; margin: 0;">Oleh : <a href="/profile/{{ $quote->user->id }}"> {{ $quote->user->name }}</a>
							        		<hr style="margin-top:0px;">
							        		<p style="font-size: 12px;"> Tag :
							        			@foreach($quote->tags as $tag)
													<span>{{ $tag->name }}</span>
							        			@endforeach
							        		</p>
							        		<a href="/quotes/{{ $quote->slug }}" class="btn btn-sm btn-explore">Baca</a>
							      		</div>
							    	</div>
								</div>
						  @endforeach
						    </div>
						</div>
						<div class="col-12 col-md-3" style="border-left: 1px solid #def2f1;">
							<div class="search">
								<h5>Search</h5>
								<form action="/quotes" method="GET">
									<div class="form-group">
										<input type="text" placeholder="Search" class="form-control" name="search">
									</div>
									<button type="submit" class="btn btn-default">Submit</button>
								</form>
							</div>
							<div class="post-category" style="margin-top: 25px;">
								<h5>By Tags</h5>
								<ul class="list-unstyled">
									@foreach($tags as $tag)
										<li><a href="/quotes/filter/{{ $tag->name }}">{{ $tag->name }}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
				  </div>
		</div>
	</div>
@endsection