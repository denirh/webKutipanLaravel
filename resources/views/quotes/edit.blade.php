@extends('layouts.app')

@section('content')
<div class="container create-post">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 form-create">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/quotes/{{ $quote->id }}" method="POST">
                <h2 class="text-center">Edit Postingan</h2>
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" name="title" placeholder="Isikan Judul" value="{{ (old('title')) ? old('title') : $quote->title }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="subject">Deskripsi</label>
                    <textarea name="subject" cols="80" rows="8" placeholder="Subject" class="form-control">{{ (old('subject')) ? old('subject') : $quote->subject }}</textarea>
                </div>
                <div id="tag_wrapper">
                    <label for="">Tag (min.1 | max. 3)</label>
                    <div id="add_tag" style="margin-bottom: 7px; color: blue; cursor: pointer; width: 20%;">Add Tag</div>
                   
                    @foreach($quote->tags as $oldtags)
                        <select name="tags[]" id="tag_select">
                                <option value="0">None</option>
                            @foreach($tags as $tag)
                                <option value="{{ $tag->id }}" 
                                    @if( $oldtags->id == $tag->id )
                                        selected="selected"
                                    @endif
                                    >{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    @endforeach

                  <script type="text/javascript" src="{{ asset('js/tag.js') }}"></script>
                </div>
                {{ csrf_field() }}

                <input name="_method" type="hidden" value="PUT">
                <button type="submit" class="btn btn-posting" style="margin-top: 15px;">EDIT</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection
