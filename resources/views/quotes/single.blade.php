@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row padding-single">
			<div class="col-md-8">
				<div class="single-post single-page">
					<h3>{{ $quote->title }}</h3>
					<p style="font-size: 12px; padding-bottom: 11px;">Ditulis oleh : <a href="/profile/{{ $quote->user->id }}"> {{ $quote->user->name }}</a></p>
					<p>{{ $quote->subject }}</p>

					<div class="like_wrapper">
						<div class="btn btn-sm btn-primary btn-like" data-model-id="{{ $quote->id }}" data-type="1">like</div>
						<div class="total_like" style="font-size: 11px;">
							0 Total like
						</div>
					</div>

					@if($quote->isOwner())
						<div style="display: flex;">
							<a href="/quotes/{{ $quote->id }}/edit" class="button-edit">Edit</a>
							<form action="/quotes/{{ $quote->id }}" method="POST">
								{{ csrf_field() }}
								<input name="_method" type="hidden" value="DELETE">
	                			<button type="submit" class="button-delete">Hapus</button>
							</form>
						</div>
					@endif

				</div>
					
			</div>
			<div class="col-md-4">
					<div class="comment single-page">
						@if(session('msg'))
							<div class="alert alert-success">
								<p>{{ session('msg') }}</p>
							</div>
						@endif
						<p>Daftar Komentar</p>
						<hr>

						@foreach($quote->comments as $comment)
                            <p style="font-size: 13px;"><a href="/profile/{{ $comment->user->id }}"> {{ $comment->user->name }}</a></p>
                            <p>{{ $comment->subject }}</p>                            

                            @if($comment->isOwner())
								<div style="display: flex;">
									<div class="edit">
										<a href="/quotes-comment/{{ $comment->id }}/edit" class="button-edit">Edit</a>
									</div>
									<form method="POST" action="/quotes-comment/{{ $comment->id }}" >
										{{ csrf_field() }}
										<input name="_method" type="hidden" value="DELETE">
			                			<button type="submit" class="button-delete">Hapus</button>
									</form>

									{{-- <div class="like_wrapper">
										<div class="btn btn-sm btn-primary btn-like" data-model-id='{{ $comment ->id }}' data-type='2'>like</div>
										<div class="total_like" style="font-size: 11px;">
											0 Total like
										</div>
									</div> --}}

								</div>
							@endif
							<hr>
                        @endforeach

						 @if(count($errors) > 0)
			                <div class="alert alert-danger">
			                    <ul>
			                        @foreach($errors->all() as $error)
			                            <li>{{ $error }}</li>
			                        @endforeach
			                    </ul>
			                </div>
			             @endif

			             <form action="/quotes-comment/{{ $quote->id }}" method="POST">
							<div class="form-group">
			                    <label for="subject">Isi Komentar</label>
			                    <textarea name="subject" cols="20" rows="2" placeholder="Komentar" class="form-control">{{ old('subject') }}</textarea>
			                </div>
		                {{ csrf_field() }}
		                <button type="submit" class="btn btn-posting">Komentar</button>
					</div>
						</form>
			</div>
		</div>

		<script src="{{ asset('js/quote.js') }}" charset="utf-8"></script>
	</div>
@endsection