@extends('layouts.app')

@section('content')
<div class="container create-post">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 form-create">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('tag_error'))
                    <div class="alert alert-danger">
                        <p>{{ session('tag_error') }}</p>
                    </div>
                @endif
            <form action="/quotes" method="POST">
                <h2 class="text-center">Buat Postingan Baru</h2>
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" name="title" placeholder="Isikan Judul" value="{{ old('title') }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="subject">Deskripsi</label>
                    <textarea name="subject" cols="80" rows="8" placeholder="Subject" class="form-control">{{ old('subject') }}</textarea>
                </div>
                <div id="tag_wrapper">
                    <label for="">Tag (min.1 | max. 3)</label>
                    <div id="add_tag" style="margin-bottom: 7px; color: blue; cursor: pointer; width: 20%;">Add Tag</div>
                    <select name="tags[]" id="tag_select">
                            <option value="0">None</option>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                  <script type="text/javascript" src="{{ asset('js/tag.js') }}"></script>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-posting" style="margin-top: 15px;">POSTING</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection
