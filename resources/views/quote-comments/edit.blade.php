@extends('layouts.app')

@section('content')
<div class="container create-post">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 form-create">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/quotes-comment/{{ $comment->id }}" method="POST">
                <h2 class="text-center">Edit Komentar</h2>
                <div class="form-group">
                    <label for="subject">Deskripsi</label>
                    <textarea name="subject" cols="80" rows="8" placeholder="Subject" class="form-control">{{ (old('subject')) ? old('subject') : $comment->subject }}</textarea>
                </div>
                {{ csrf_field() }}

                <input name="_method" type="hidden" value="PUT">
                <button type="submit" class="btn btn-posting">EDIT</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
@endsection
